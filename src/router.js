import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

import NotFound from './pages/notFound'
import homePage from './pages/homePage'

export default new Router({
    mode: 'history',
    routes: [ 
        {
            path: '/404',
            name: '404',
            component: NotFound,
        },
        {
            path: '*',
            redirect: '/404'
        },
        {
            path: '/',
            name: 'homePage',
            component: homePage,
        },
    ]
})
